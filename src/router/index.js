import Vue from 'vue'
import Router from 'vue-router'

import Eventos from '@/components/Eventos'
import Evento from '@/components/Evento'
import Relatorio from '@/components/Relatorio'
import Ingressos from '@/components/Ingressos'
import TipoIngressos from '@/components/TipoIngressos'
import Clientes from '@/components/Clientes'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: 'relatorio'
    },
    {
      path: '/relatorio',
      name: 'relatorio',
      component: Relatorio,
    },
    {
      path: '/evento',
      name: 'eventos',
      component: Eventos,
    },
    {
      path: '/evento/:id',
      name: 'evento',
      component: Evento,
    },
    {
      path: '/ingressos',
      name: 'ingressos',
      component: Ingressos,
    },
    {
      path: '/ingressos-tipo',
      name: 'tipoIngressos',
      component: TipoIngressos,
    },
    {
      path: '/clientes',
      name: 'clientes',
      component: Clientes,
    },
  ]
})
